package controlador;

import com.example.pdm_atividade1.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

/**
 * @author Joew
 * 
 * Esta classe representa o controlador da tela inicial da aplica��o.
 * Sua fun��o � definir o comportamento dos elementos definidos nesta tela, estabelecendo assim como se dar� a intera��o com o usu�rio.
 *
 */

public class MainActivity extends Activity{
	
	//m�todo que cria a activity, sua fun��o � "montar a tela" e apresent�-la ao usu�rio.
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		//este m�todo recebe a informa��o das configura��es de layout definidos no arquivo xml correspondente (par�metro necess�rio)
		setContentView(R.layout.app_home);
		
		
		//criando refer�ncia aos elementos (Views) de tela, para poderem ser manipulados
		ImageView imvTarefas = (ImageView) findViewById(R.id.imvTarefas);
		
		//setando um evento para monitorar o momento em que o usu�rio clicar na imagem tarefas
		imvTarefas.setOnClickListener( new OnClickListener() {
			
			//m�todo que define a tarefa a ser realizada ap�s o clique na imagem tarefas
			@Override
			public void onClick(View arg0) {
				//Criando uma inten��o para a redirecionamento para a tela de tarefas
				Intent intent = new Intent(MainActivity.this, TarefasActivity.class);
				startActivity(intent);
				
			}
		});
		
	}
}
