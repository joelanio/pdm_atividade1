package controlador;

import com.example.pdm_atividade1.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;



/**
 * @author Joew
 * 
 * Esta classe representa o controlador da tela da lista de tarefas.
 * Sua fun��o � definir o comportamento dos elementos definidos nesta tela, estabelecendo assim como se dar� a intera��o com o usu�rio.
 *
 */

public class TarefasActivity extends Activity {

	//m�todo que cria a activity, sua fun��o � "montar a tela" e apresent�-la ao usu�rio.
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//este m�todo recebe a informa��o das configura��es de layout definidos no arquivo xml correspondente (par�metro necess�rio)
		setContentView(R.layout.tarefas);
		
		//criando refer�ncia aos elementos (Views) de tela, para poderem ser manipulados
		Button btDashboard = (Button) findViewById(R.id.btDashboard);
		Button btVernotas = (Button) findViewById(R.id.btVerNotas);
		
		//setando um evento para monitorar o momento em que o usu�rio clicar no bot�o dashboard
		btDashboard.setOnClickListener(new OnClickListener() {
			
			//m�todo que define a tarefa a ser realizada ap�s o clique do bot�o dashboard
			@Override
			public void onClick(View v) {
				//Criando uma inten��o para a redirecionamento para a tela principal(Home) 
				Intent intent = new Intent(TarefasActivity.this, MainActivity.class);
				startActivity(intent);
				finish();
				
			}
		});
		
		//setando um evento para monitorar o momento em que o usu�rio clicar no bot�o verNotas
		btVernotas.setOnClickListener(new OnClickListener() {
			
			//m�todo que define a tarefa a ser realizada ap�s o clique do bot�o dashboard
			@Override
			public void onClick(View v) {
				//como n�o h� implementa��o desta funcionalidade, a aplica��o manda apenas uma mensagem ao usu�rio informando que ainda falta a implementa��o.
				Toast.makeText(TarefasActivity.this, "Fun��o n�o implementada", Toast.LENGTH_LONG).show();
				
			}
		});
		
	}
}
